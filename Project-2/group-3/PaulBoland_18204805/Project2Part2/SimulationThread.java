package historydata;

import java.text.ParseException;
import java.util.List;

import org.jfree.data.Value;
import org.jfree.data.Values;

public class SimulationThread extends Thread {

	private static final int DELAY_IN_MS = 10;

	private final List<ServiceAdapter<?>> services;
	private HistoryData currentInputData;
	private ProcessData processor;
	private int counter = 1;
	

	public SimulationThread(List<ServiceAdapter<?>> services) {
		this.services = services;
	}

	/**
	 * History Data
	 */
	@Override
	public void run() {
		try {
			
			setupFiles();
			//While we have data in file to be processed
			while (counter < processor.getSize()) {
				
				/* Process the data */
				ProcessDataRow(counter);
				if(currentInputData!=null)
				{
					System.out.println(this.currentInputData);
	
					/* Use services */
					for (ServiceAdapter<?> s : services)
						s.post(currentInputData);
	
					/* Wait a bit */
					Thread.sleep(DELAY_IN_MS);
					counter++;
				}
			}
			System.out.println("\n\n************************\n"
					+ "Classification Complete \n"
					+"************************");
		} catch (InterruptedException e) {
		}
	}
	
	/**
	 * Select file for testing
	 */
	private void setupFiles() {
		CSVReader reader = new CSVReader();
		List<String[]> data;
		
		if(reader.isFileFound()) {
			data = reader.getData();
			this.processor = new ProcessData(data);
		}
	}
	
	/**
	 * 
	 * ----------------------------
	 * FORMAL CHECKER OF DATA INPUT
	 * ----------------------------
	 * TODO Change index based on actual data sheet for Historical Data
	 * @throws ParseException 
	 */
	public void ProcessDataRow(int counter){
		currentInputData = new HistoryData();
		String[] values = processor.getCurrentRow(counter);
		
		/* if the last few items are blank the extraction will truncate these items.
		 * Re-add these if needed 
		 * */
		if(values.length < 24)
			values = ExtendDataRowArray(values, 24);
				
		//ID
		//currentInputData.setIsNumeric(values[0]);
		currentInputData.setIsValidIDFormat(values[0]);
		
		// District
		currentInputData.setIsDistrictRecorded(values[1]);
		
		// DeathDate
		currentInputData.setIsDeathDateOutOfRange(values[2]);
		
		// Place of Death
		currentInputData.setIsValidPlaceOfDeath(values[3]);
		
		//Sex
		currentInputData.setIsValidSex(values[5]);
		
		//Deceased Forename
		currentInputData.setIsValidDeceasedForename(values[6]);
		
		//Deceased Surname
		currentInputData.setIsValidDeceasedSurname(values[7]);
		
		//Age
		currentInputData.setIsValidAge(values[8]);
		
		//Martial Status
		currentInputData.setMaritalStatus(values[9]);
		
		// District REg Area
		currentInputData.setIsValidDistrictRegArea(values[10]);
		
		// District REg Area
		currentInputData.setIsValidOccupation(values[11]);
				
		//Cause of Death
		currentInputData.setCauseOfDeathInput(values[12]);
		currentInputData.setSecondaryCauseOfDeathInput(values[14]);
		
		//Certification
		currentInputData.setCertification(values[16]);
		
		//Valid Informant Present at Death
		currentInputData.setIsValidInformant(values[17]);
		currentInputData.setIsPresentAtDeathInput(values[19]);
		
		//Qualified Informant or Address of Informant
		currentInputData.setIsQualifiedInformant(values[18]);
		currentInputData.setIsValidInformantAddress(values[20]); 
		
		//Registration Data
		currentInputData.setIsRegistrationDateOutOfRange(values[21]);
		
		//isValidRegistrarName
		currentInputData.setIsValidRegistrarName(values[22]);
		
		//setIsTiffFileProvided
		currentInputData.setIsTiffFileProvided(values[23]);
	}
	
	private String[] ExtendDataRowArray(String[] dataRowArray, int totalRequiredLength)
	{
		
		// create a new array of size totalLength
		String[] extendedDataRowArray = new String[totalRequiredLength];
		
		// populate the new array with the values from the existing array
		for (int i=0; i< dataRowArray.length; i++)
				extendedDataRowArray[i] = dataRowArray[i];
		
		// return the new array
		return extendedDataRowArray;
	}
	
}
