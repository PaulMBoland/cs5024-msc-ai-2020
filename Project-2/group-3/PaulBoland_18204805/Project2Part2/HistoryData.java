package historydata;


import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter; 
import java.time.format.DateTimeParseException;

import historydata.Predicates;

/**
 * Change Predicates
 * 
 * @author PC
 *
 */
public class HistoryData implements Predicates {
	
	static String idUniqueRef="000000";

	private boolean isValidIDFormat, isNumeric, isMale, isFemale, isValidAge, isMarried, isBachelor, isWidower, isSingle, 
	isPresentAtDeath, isNotPresentAtDeath, isValidInformant,
	isQualifiedInformant, isValidInformantAddress, isRegistrationOutOfDate, isCertified, isUncertified
	,isDistrictRecorded, isDeathDateOutOfDate, isValidPlaceOfDeath, isValidDeceasedForename, isValidDeceasedSurname
	,isValidDistrictRegArea, isValidOccupation,isValidCauseOfDeath,isValidRegistrarName,isTiffFileProvided,isValidResidence,isUniqueIDString,isSecondaryValidCauseOfDeath;
	private StringBuilder builder;
	private LocalDate checkedDate;

	
	
	// Enums the given occupations, note spaces are replaced with _
	private enum  Profession_or_Occupation 
	{
	Teacher,Farmer,Lady,Labourer,Housekeeper,Baker;
		
	public static boolean contains(String s)
	  {
	      for(Profession_or_Occupation option:values())
	           if (option.name().toLowerCase().equals(s.trim().toLowerCase().replace(" ", "_"))) // Cleans the data
	              return true;
	      return false;
	  } 
	};
		
	// Enums the given values for cause of death, note spaces are replaced with _
	private enum  causesOfDeath
	{
	Asthenia,Meningitis,Senility,Phthisis,Debility,Cardiac_Failure;
		
	public static boolean contains(String s)
	  {
	      for(causesOfDeath option:values())
	           if (option.name().toLowerCase().equals(s.trim().toLowerCase().replace(" ", "_"))) // Cleans the data
	              return true;
	      return false;
	  } 
	};
	
	public HistoryData() {
		builder = new StringBuilder();
		builder.append("\n---------------------\nHistory Input:\n");
		builder.append("1.Registration ID\n");
	}

	@Override
	public String toString() {
		return builder.toString();
	}

	/**
	 * Check if value is numeric 
	 * Set isNumeric boolean predicate
	 * 
	 * @param id
	 */
	public void setIsNumeric(String input) {
		if (isNullEntry(input))
				isNumeric = false;
		try {
			Long.parseLong(input);
			isNumeric = true;
		} catch (NumberFormatException nfe) {
			isNumeric = false;
		}
		
		builder.append("	isNumeric: " + isNumeric + "\n");
	}

	@Override
	public boolean isNumeric() {
		return isNumeric;
	}
	
	/**
	 * Set Valid Format boolean for IDs Currently only values less that 10 million
	 * are valid (removes student IDs)
	 * Also checks that the previous ID value is different to avoid accidental duplicates
	 * XLS must be sorted on column 0 for this feature to work efficiently
	 * @param id
	 */
	public void setIsValidIDFormat(String id) {
		setIsNumeric(id);
		if (isNumeric) {
			if (Long.parseLong(id.trim()) < 10000000)
				isValidIDFormat = true;
		}
		
		builder.append(" 	isValidIDFormat:: " + isValidIDFormat + "  -> " + id + "\n");
		
		if (!id.equals(idUniqueRef))
		{
			isUniqueIDString = true;
		
		}
		
		builder.append(" isUniqueIDString: " + isUniqueIDString+ ": Previous ID string is "+idUniqueRef+" Current  id is: " + id + "\n");
		idUniqueRef=id; // sets the static variable so it can be checked on next iteration
	}

	@Override
	public boolean isValidIDFormat() {
		return isValidIDFormat;
	}

	
	/**
	 * Check if the district information has been provided
	 */
	public void setIsDistrictRecorded(String input) {
		builder.append("\n Distict Recorded\n");
		if(!isNullEntry(input))
			isDistrictRecorded = true;
		
		builder.append("	isDistrictRecorded: " + isDistrictRecorded + "\n");
	}
	
	
	@Override
	public boolean isDistrictRecorded() {
		return isDistrictRecorded;
	}

	
	/**
	 * Retrieve date string
	 * Convert to correct format
	 * Check if numeric
	 * Check if within timeframe
	 * @param input
	 * @throws ParseException
	 */
	public void setIsDeathDateOutOfRange(String input) {
		builder.append("\n Death Date\n");
		
		 
		isDeathDateOutOfDate = true;
		boolean isDate = false;

		// check the given value is a valid date
		isDate = CheckDate(input);

		if(isDate) { // check is within years allowed
			long year = checkedDate.getYear();
			if(year >= 1863 && year <= 1922)
				isDeathDateOutOfDate = false;
		}	
		builder.append(" 	isDeathDateOutOfDate: " + isDeathDateOutOfDate +"\n");
	}

	@Override
	public boolean isDeathDateOutOfRange() {
		return isDeathDateOutOfDate;
	}

	/**
	 * Check if the place of death has been provided
	 */
	public void setIsValidPlaceOfDeath(String input) {
		builder.append("\n Distict Recorded\n");
		if(!isNullEntry(input))
			isValidPlaceOfDeath = true;
		
		builder.append("	isValidPlaceOfDeath: " + isValidPlaceOfDeath + "\n");
	}
	
	
	@Override
	public boolean isValidPlaceOfDeath() {
		return isValidPlaceOfDeath;
	}	
		
	/**
	 * Set the Sex of the Input
	 * @param text
	 */
	public void setIsValidSex(String input) {		
		if(!isNullEntry(input))
		{	
			if(input.trim().toLowerCase().equals("m"))
				isMale = true;
			
			if(input.trim().toLowerCase().equals("f"))
				isFemale = true;
		}
		builder.append("\n Sex\n 	isMale: " + isMale + "\n" + " 	isFemale: " + isFemale + "\n");
	}
	
	
	@Override
	public boolean isMale() {
		return isMale;
	}
		
	@Override
	public boolean isFemale() {
		return isFemale;
	}
	
	
	/**
	 * Check if the residence has been provided
	 */
	public void setIsValidResidence(String input) {
		builder.append("\n Residence\n");
		if(!isNullEntry(input))
			isValidResidence = true;
		
		builder.append("	isValidResidence: " + isValidResidence + "\n");
	}
	
	
	@Override
	public boolean isValidResidence() {
		return isValidResidence;
	}	
	
	
	
	/**
	 * Check if the Forename has been provided
	 */
	public void setIsValidDeceasedForename(String input) {
		builder.append("\n Deceased Forename\n");
		if(!isNullEntry(input))
			isValidDeceasedForename = true;
		
		builder.append("	isValidDeceasedForename: " + isValidDeceasedForename + "\n");
	}
	
	
	@Override
	public boolean isValidDeceasedForename() {
		return isValidDeceasedForename;
	}	
		
	
	/**
	 * Check if the Surname has been provided
	 */
	public void setIsValidDeceasedSurname(String input) {
		builder.append("\n Deceased Surname\n");
		if(!isNullEntry(input))
			isValidDeceasedSurname = true;
		
		builder.append("	isValidDeceasedSurname: " + isValidDeceasedSurname + "\n");
	}
	
	
	@Override
	public boolean isValidDeceasedSurname() {
		return isValidDeceasedSurname;
	}	

	
	/**
	 * Check if Age value is valid
	 * @param age
	 */
	public void setIsValidAge(String input) {
		builder.append("\n Deceased Age\n");
		setIsNumeric(input);
		if(isNumeric) {
			long age  = Long.parseLong(input.trim());
			if(age <= 130 && age >=0)
				isValidAge = true;
		}	
		builder.append(" 	isValidAge: " + isValidAge + "\n");
	}
	
	
	@Override
	public boolean isValidAge() {
		return isValidAge;
	}
	
	
	/**
	 * Check if Civil Status is valid
	 * {M,W,B,S}
	 * @param text
	 */
	public void setMaritalStatus(String input) {
		builder.append("\n Civil Status\n");
		if(!isNullEntry(input))
		{	
			if(input.trim().toLowerCase().equals("m"))
				isMarried = true;
			
			if(input.trim().toLowerCase().equals("w"))
				isWidower = true;
			
			if(input.trim().toLowerCase().equals("b"))
				isBachelor = true;
			
			if(input.trim().toLowerCase().equals("s"))
				isSingle = true;
		}
		
		builder.append("\n 	isMarried: " + isMarried + "\n" + " 	isWidower: " + isWidower + "\n" + " 	isBachelor: " + isBachelor + "\n" + " 	isSpinster: " + isSingle + "\n");
	}

	@Override
	public boolean isMarried() {
		return isMarried;
	}

	@Override
	public boolean isWidower() {
		return isWidower;
	}


	@Override
	public boolean isSingle() {
		return isSingle;
	}
	@Override
	public boolean isBachelor() {
		return isBachelor;
	}
	

	
	/**
	 * Check if the District or registered area has been provided
	 */
	public void setIsValidDistrictRegArea(String input) {
		builder.append("\n SR_District_Reg_Area\n");
		if(!isNullEntry(input))
			isValidDistrictRegArea = true;
		
		builder.append("	isValidDistrictRegArea: " + isValidDistrictRegArea + "\n");
	}
	
	
	@Override
	public boolean isValidDistrictRegArea() {
		return isValidDistrictRegArea;
	}	
			

	/**
	 * Check if the profession or occupation has been provided and is one of the allowed values
	 */
	public void setIsValidOccupation(String input) {
		builder.append("\n Profession_or_Occupation \n");
		if(!isNullEntry(input))
		{
			// check if the given value is one of the allowed values in the XLS
			if(Profession_or_Occupation.contains(input))
				isValidOccupation = true;
		}
		
		builder.append("	isValidOccupation: " + isValidOccupation + "\n");
	}
	
	
	@Override
	public boolean isValidOccupation() {
		return isValidOccupation;
	}	
				
	
	/**
	 * Check if input if valid for Primary Cause of Death
	 * @param text
	 */
	public void setCauseOfDeathInput(String input) {
		builder.append("\n Primary Cause of Death \n");
		if(!isNullEntry(input))
			if(causesOfDeath.contains(input))
				isValidCauseOfDeath = true;
		
				
		builder.append("\n 	isValidCauseOfDeath: " + isValidCauseOfDeath + "\n");
	}
	
	@Override
	public boolean isValidCauseOfDeath() {
		return isValidCauseOfDeath;
	}
	
	/**
	 * Check if input if valid for Secondary Cause of Death
	 * @param text
	 */
	public void setSecondaryCauseOfDeathInput(String input) {
		builder.append("\n Secondary Cause of Death \n");
		if(!isNullEntry(input))
			if(causesOfDeath.contains(input))
				isSecondaryValidCauseOfDeath = true;
		
		
		
		builder.append("\n 	isValidCauseOfDeath: " + isValidCauseOfDeath + "\n");
	}
	
	@Override
	public boolean isSecondaryValidCauseOfDeath() {
		return isSecondaryValidCauseOfDeath;
	}
	
	
	/**
	 * Check certification value, should be U or C
	 * @param text
	 */
	public void setCertification(String input) {
		builder.append("\n Certification \n");
		if(!isNullEntry(input))
		{	
			if(input.trim().toLowerCase().equals("c"))
				isCertified = true;
			
			if(input.trim().toLowerCase().equals("u"))
				isUncertified = true;
		}
		
		builder.append("\n isCertified: " + isCertified + "\n" + "	isUncertified: " + isUncertified + "\n");
	}

	@Override
	public boolean isUncertified() {
		return isUncertified;
	}

	@Override
	public boolean isCertified() {
		return isCertified;
	}
	
	/**
	 * Check if Informant is Valid
	 * @param text
	 */
	public void setIsValidInformant(String input) {
		builder.append("\n Name of Informant\n ");
		if(!isNullEntry(input))	
			isValidInformant = true;
		
		builder.append("\n	isValidInformant: " + isValidInformant + "\n");
	}
	
	@Override
	public boolean isValidInformant() {
		return isValidInformant;
	}
	
	
	/**
	 * Check if Informant was present
	 * @param text
	 */
	public void setIsPresentAtDeathInput(String input) {
		builder.append("\n Informant was Present\n ");
		if(!isNullEntry(input))
		{	
			if(input.trim().toLowerCase().equals("p"))
				isPresentAtDeath = true;
			
			if(input.trim().toLowerCase().equals("n"))
				isNotPresentAtDeath = true;
		}
		
		builder.append("\n 	isValidInformant: " + isValidInformant + "\n" + "	isPresentAtDeath: " + isPresentAtDeath + "\n"
				+ "	isNotPresentAtDeath: " + isNotPresentAtDeath + "\n");
	}
	

	@Override
	public boolean isPresentAtDeath() {
		return isPresentAtDeath;
	}
	
	@Override
	public boolean isNotPresentAtDeath() {
		return isNotPresentAtDeath;
	}


	/**
	 * Check if Informant has a value
	 */
	public void setIsQualifiedInformant(String input) {
		builder.append("\n Qualified  Informant\n ");
		if(!isNullEntry(input))
			isQualifiedInformant = true;
		
		builder.append("\n	isQualifiedInformant: " + isQualifiedInformant + "\n");
	}
	
	@Override
	public boolean isQualifiedInformant() {
		return isQualifiedInformant;
	}
	
	/**
	 * Check if informant has a valid address
	 */
	public void setIsValidInformantAddress(String input) {
		builder.append("\n Informant Address\n ");
		if(!isNullEntry(input))
			isValidInformantAddress = true;
		
		builder.append("	isValidInformantAddress: " + isValidInformantAddress + "\n");
	}
	
	
	@Override
	public boolean isValidInformantAddress() {
		return isValidInformantAddress;
	}

	/**
	 * Retrieve date string
	 * Convert to correct format
	 * Check if numeric
	 * Check if within timeframe
	 * @param input
	 * @throws ParseException
	 */
	public void setIsRegistrationDateOutOfRange(String input) {
		builder.append("\n Registration Date\n");
		
		
		isRegistrationOutOfDate = true;
		boolean isDate = false;

		// check the given value is a valid date
		isDate = CheckDate(input);

		if(isDate) {  // check the years
			long year = checkedDate.getYear();
			if(year >= 1863 && year <= 1922)
				isRegistrationOutOfDate = false;
		}	
		builder.append(" 	isRegistrationOutOfDate: " + isRegistrationOutOfDate +"\n");
	}

	@Override
	public boolean isRegistrationDateOutOfRange() {
		return isRegistrationOutOfDate;
	}
	

	/**
	 * Check if the name of the registrar has been provided
	 */
	public void setIsValidRegistrarName(String input) {
		builder.append("\n Name of Registrar\n");
		if(!isNullEntry(input))
			isValidRegistrarName = true;
		
		builder.append("	isValidRegistrarName: " + isValidRegistrarName + "\n");
	}
	
	
	@Override
	public boolean isValidRegistrarName() {
		return isValidRegistrarName;
	}
	

	/**
	 * Check if there is a Tiff link provided with the record
	 */
	public void setIsTiffFileProvided(String input) {
		builder.append("\n Tiff URL\n");
		if(!isNullEntry(input))
			isTiffFileProvided = true;
		
		builder.append("	isTiffFileProvided: " + isTiffFileProvided + "\n");
	}
	
	
	@Override
	public boolean isTiffFileProvided() {
		return isTiffFileProvided;
	}
	
	
	/**
	 * Check if input is Null or Empty string
	 * @param text
	 * @return
	 */
	public boolean isNullEntry(String input) {
		if(input==null||input.equals(""))
			return true;
		
		return false;
	}
	

	/**
	 * Check if input is a valid date
	 * @param text
	 * @return
	 */
	public boolean CheckDate(String stringToTest) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM uuuu");
		// ensure the value is not null or empty
		if(isNullEntry(stringToTest))
			return false;
		
		// try to parse the string as a date
		try {
			checkedDate = LocalDate.parse(stringToTest, formatter);
	        // if successful then return true
	        return true;
	    } catch (DateTimeParseException dtpe) {
	    	// if an exception is raised then return false
	    	return false;
	    }
	}


}
